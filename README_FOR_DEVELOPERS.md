# README FOR DEVELOPERS

## Generate parser from grammar

`python setup.py generate`

## Execute tests

`python setup.py test`

