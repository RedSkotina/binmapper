# encoding: binmapper
import io

bintype BinX:
    field_uint16: uint16

bintype Bin:
    field: BinX

def test():
    bin = Bin()
    bin.parse(io.BytesIO("    ÿþÿýÿÿÿ   @"))
    if bin.field.field_uint16.value == 2:
        return True
    return False
